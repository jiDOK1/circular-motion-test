using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    // yOffset muss noch mit playerT verrechnet werden
    float yOffset;
    float distance;
    Vector3 center = new Vector3();
    Transform playerT;

    void Start()
    {
        playerT = GameObject.Find("Player").transform;
        yOffset = transform.position.y;
        distance = Vector3.Distance(playerT.position, transform.position);
    }

    void Update()
    {
        Vector3 dir = playerT.position - center;
        dir.Normalize();
        Vector3 xzPos = center + dir * distance;
        transform.position = new Vector3(xzPos.x, yOffset, xzPos.z);
        transform.LookAt(playerT);
    }
}
