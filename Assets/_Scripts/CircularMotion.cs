using UnityEngine;

public class CircularMotion : MonoBehaviour
{
    public float Radius = 3f;
    public float Speed = 40f;
    public LayerMask layerMask;
    CharacterController cc;
    Rigidbody rb;
    float inputX = 0f;
    Vector3 center;
    Vector3 newDir;

    void Awake()
    {
        cc = GetComponent<CharacterController>();
        center = new Vector3();
        transform.position = Vector3.right * Radius;
        gameObject.SetActive(true);
    }

    void Update()
    {
        Move();
    }

    void Move()
    {
        Debug.DrawRay(transform.position + Vector3.up, newDir * 30f, Color.blue);
        float iX = Input.GetAxis("Horizontal") * Time.deltaTime;
        inputX += iX;
        if (GetNewDir(inputX).sqrMagnitude > 0f)
        {
            Ray ray = new Ray(transform.position, GetNewDir(inputX));
            if (Physics.Raycast(ray, 1f, layerMask, QueryTriggerInteraction.Ignore))
            {
                inputX -= iX;
            }
        }
        //inputX = inputX % 1f;
        newDir = GetNewDir(inputX);
        cc.Move(newDir);
    }

    Vector3 GetNewDir(float inputX)
    {
        float newX = Mathf.Cos(inputX);
        float newZ = Mathf.Sin(inputX);
        var xzPos = new Vector3(transform.position.x, 0f, transform.position.z);
        var newPos = new Vector3(newX * Radius, 0f, newZ * Radius);
        newDir = newPos - xzPos;
        return newDir;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(center, Radius);
    }
}
